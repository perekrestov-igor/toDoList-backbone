$(document).ready(function(){

    window.App = {
        Models: {},
        Views: {},
        Collections: {}
    };

    window.template = function(id) {
        return _.template( $('#' + id).html() );
    };

    //start constants and variables
    var time_slide_effect = 500;//ms
    var time_slide_effect_multiplier = 3;
    var tmp_this;
    //end constants and variables

    //start common functions
    function show_placeholder_if_section_empty(section, direction) {
        if($("."+section).find(".task_module").length === 0) {
            setTimeout(function() {
                $("."+section+" .ui-effects-wrapper").remove();
                $("."+section+" .placeholder").show( "slide", { direction: direction }, time_slide_effect * 3 );
            }, 100);
            return true;
        }
        return false;
    }

    function hide_placeholder(section,direction) {
        if($("."+section+" .placeholder").css('display') === "block") {
            $("."+section+" .placeholder").hide("slide", { direction: direction }, time_slide_effect * time_slide_effect_multiplier );
            return true;
        }
        return false;
    }

    var taskImportantMark = {
        toggle: function(tmp_this) {
            if ( this.get(tmp_this)) {
                this.set(tmp_this,false);
            } else {
                this.set(tmp_this,true);
            }
        },
        set: function(tmp_this,direction) {
            var $target = tmp_this.$el.find(".ui-icon-star");
            if ( direction ) {
                $target.css("background-image", "url(./images/jquery-ui/ui-icons_cd0a0a_256x240.png)");
            } else {
                $target.css("background-image", "url(./images/jquery-ui/ui-icons_222222_256x240.png)");
            }
        },
        get: function(tmp_this) {
            var $target = tmp_this.$el.find(".ui-icon-star");
            return ($target.css("background-image").slice(-19,-13) === "cd0a0a");
        }
    };

    App.Models.task = Backbone.Model.extend({
        defaults: {
            title: "task's title",
            comment: "...You didn`t type comment for this task.",
            important_flag: false,
            data_start: "09/29/1985",
            data_end: "09/29/1985",
            section: "in_work",
            task_number: 0
        },
        initialize: function() {
            this.on('change', this.change, this);
            this.on('destroy', this.remove, this);
        },
        change: function() {
            localStorage.setItem("toDoList_task_number_" + this.get('task_number'), JSON.stringify(this.toJSON()));
        },
        remove: function() {
            localStorage.removeItem("toDoList_task_number_" + this.get('task_number'));
        }
    });

    App.Views.task = Backbone.View.extend({
        tagName: 'div',
        className: 'task_module',

        template:  template('blank2'),
        template_editing_mode: template('blank2_editing_mode'),

        events:{
            'click .ui-icon-star'           : 'taskImportantMark_click',
            'click .task_button_edit'       : 'editTask',
            'click .task_button_cancel'     : 'cancelTask',
            'click .task_button_done'       : 'doneTask',
            'click .task_button_delete'     : 'confirmation_deleteTask',
            'click .task_button_backInWork' : 'backInWorkTask',
            'click .task_button_showComment': 'showComment'
        },

        editing_mode: false,
        showing_comment_mode: false,
        important_flag: false,

        initialize: function() {
            this.initialization_render();
            this.model.on("change:section",this.hideTaskAndShowIn,this);
        },

        initialization_render: function() {
            this.switchToStaticElements_render();
            var section = this.model.get('section');
            this.$el.css("display","none");
            this.$el.insertAfter("." + section + " .placeholder");
            var direction = this.direction(section).inv_direction;
            this.$el.show( "slide", { direction: direction }, time_slide_effect * 3 );
            return this;
        },
        switchToStaticElements_render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            this.$el.find(".task_comment").hide();
            taskImportantMark.set(this,this.model.get("important_flag"));
            return this;
        },
        switchToEditableElements_render: function() {
            function set_date_in_datepicker(str) {
                var str_date = tmp_this.model.get("data_" + str);
                if(tmp_this.model.get("data_" + str) !== "didn't set") {
                    var date = new Date(Number(str_date.slice(-4)), Number(str_date.slice(0, 2)) - 1, Number(str_date.slice(3, 5)));
                    tmp_this.$el.find(".task_date_" + str).datepicker('setDate', date);
                } else {
                    tmp_this.$el.find(".task_date_" + str).val(str_date);
                }
            }
            this.$el.html(this.template_editing_mode(this.model.toJSON()));
            this.$el.find(".task_date_start").datepicker();
            this.$el.find(".task_date_end").datepicker();
            tmp_this = this;
            set_date_in_datepicker("start");
            set_date_in_datepicker("end");
            taskImportantMark.set(this,this.model.get("important_flag"));

            this.$el.find("textarea.task_comment").slideDown(time_slide_effect);
            this.$el.find(".task_button_edit").css("background-color", "#eeeebb");
            var task_module_color = tmp_this.$el.css("background-color");
            this.$el.find(".task_button_cancel")
                .add(this.$el.find(".task_button_done"))
                    .add(this.$el.find(".task_button_showComment"))
                        .css("background-color", task_module_color).css("cursor", "default");

            return this;
        },

        taskImportantMark_click: function() {
            if(this.editing_mode) {
                taskImportantMark.toggle(this);
            }
        },
        editTask: function() {
            if (this.editing_mode) {
                this.switchToStaticElements();
            } else {
                this.switchToEditableElements();
            }
            this.editing_mode = !this.editing_mode;
        },
        cancelTask: function() {
            if (this.show_warning_if_editing_mode_active2()) {
                return;
            }
            this.model.set('section','canceled');
        },
        doneTask: function() {
            if (this.show_warning_if_editing_mode_active2()) {
                return;
            }
            this.model.set('section','complited');
        },
        confirmation_deleteTask: function() {
            tmp_this = this;
            $("#confirmation_delete").dialog("open");
        },
        deleteTask: function() {
            this.model.set('section','eternity');
        },
        backInWorkTask: function() {
            this.model.set('section','in_work');
        },
        //editing mode
        switchToStaticElements: function() {
            this.model.set({
                "title"             : this.$el.find(".task_second_row input").val(),
                "comment"           : this.$el.find("textarea.task_comment").val(),
                "important_flag"    : taskImportantMark.get(this),
                "data_start"        : this.$el.find(".task_date_start").val(),
                "data_end"          : this.$el.find(".task_date_end").val()
            });
            var tmp_this = this;
            this.$el.find("textarea.task_comment").slideUp(time_slide_effect, function() {
                tmp_this.switchToStaticElements_render();
            });
        },
        switchToEditableElements: function() {
            if(this.showing_comment_mode) {
                var was_showing_comment_mode = this.showing_comment_mode;
                this.showComment();
            }
            var tmp_this = this;
            setTimeout(function() {
                tmp_this.switchToEditableElements_render();
            }, time_slide_effect * was_showing_comment_mode);
        },

        // common function
        direction: function(section) {
            var direction, inv_direction;
            if (section === "in_work" || section === "eternity") {
                direction = "left";
                inv_direction = "right";
            } else {
                direction = "right";
                inv_direction = "left";
            }
            return {direction: direction, inv_direction: inv_direction};
        },

        showTaskIn: function(section) {
            var direction = this.direction(section).inv_direction;
            $("." + section + " .placeholder").after(this.$el);
            this.$el.show("slide", { direction: direction }, time_slide_effect * time_slide_effect_multiplier);
        },
        hideTaskAndShowIn: function() {
            var target_section = this.model.get("section");
            var previous_section = this.model.previous("section");
            if(this.showing_comment_mode) {
                var was_showing_comment_mode = this.showing_comment_mode;
                this.showComment();
            }
            var tmp_this = this;
            setTimeout(function() {
                var direction = tmp_this.direction(target_section);
                hide_placeholder(target_section, direction.inv_direction);
                tmp_this.$el.hide("slide", {direction: direction.direction}, time_slide_effect * time_slide_effect_multiplier, function () {
                    if(target_section !== "eternity") {
                        tmp_this.showTaskIn(target_section);
                    }
                    else {
                        tmp_this.remove();
                        tmp_this.model.destroy();
                    }
                    show_placeholder_if_section_empty(previous_section, direction.direction);
                });
            }, time_slide_effect * was_showing_comment_mode);
        },
        showComment: function() {
            if (this.show_warning_if_editing_mode_active2()) {
                return;
            }
            this.showing_comment_mode = !this.showing_comment_mode;
            this.$el.find(".task_comment").slideToggle(time_slide_effect);
            this.$el.find(".task_button_showComment .ui-icon").toggleClass(function () {
                if ($(this).hasClass('ui-icon-zoomin')) {
                    return 'ui-icon-zoomout';
                } else {
                    return 'ui-icon-zoomin';
                }
            });
        },
        show_warning_if_editing_mode_active2: function() {
            if(this.editing_mode) {
                $("#warning_editing_mode").dialog("open");
                return true;
            }
            return false;
        }

    });

    //App.Collections.tasks = Backbone.Collection.extend({
    //    model: App.Models.task
    //});
    //
    //var tasksCollection = new App.Collections.tasks();


    App.Models.newTask = Backbone.Model.extend({
        defaults: {
            title: "task's title",
            comment: "...You didn`t type comment for this task.",
            important_flag: false,
            data_start: "didn't set",
            data_end: "didn't set",
            section: "in_work",
            task_number: undefined
        },

        initialize: function() {
            this.on('change', this.addingNewTask, this);
        },

        validate: function(attr) {
            if ($.trim(attr.title) === "") {
                return $("#warning_empty_title").dialog("open");
            }
        },

        addingNewTask: function() {
            var toDoList_task_counter = localStorage.getItem('toDoList_task_counter');
            var task_number_in_storage = Number(toDoList_task_counter) + 1;
            this.set("task_number",task_number_in_storage,{silent: true});
            localStorage.setItem('toDoList_task_counter',task_number_in_storage);
            localStorage.setItem("toDoList_task_number_" + task_number_in_storage, JSON.stringify(this.toJSON()));
            var task = new App.Models.task(this.toJSON());
            var taskView = new App.Views.task({model: task});
            this.clearing();
        },
        clearing: function() {
            this.set(this.defaults,{silent: true});
        }
    });


    App.Views.newTask = Backbone.View.extend({
        el: '.add_task',
        events: {
            "click .button_add_new_task": "addNewTask",
            "click .ui-icon-star"       : "toggleImportantMark"
        },

        initialize: function() {
            this.model.on("change",this.initialization_render,this);
            this.$el.find(".datepicker_start").datepicker();
            this.$el.find(".datepicker_end").datepicker();
        },

        initialization_render: function() {
            this.$el.find(".new_task_title").val("");
            this.$el.find(".new_task_comment").val("");
            this.$el.find(".datepicker_start").val("date start");
            this.$el.find(".datepicker_end").val("date end");
            taskImportantMark.set(this,false);
        },

        addNewTask: function() {
            var new_task = {};
            new_task.title = this.$el.find(".new_task_title").val();
            if($.trim(this.$el.find(".new_task_comment").val()) !== "") {
                new_task.comment = this.$el.find(".new_task_comment").val();
            }
            if(this.$el.find(".datepicker_start").val() !== "date start") {
                new_task.data_start = this.$el.find(".datepicker_start").val();
            }
            if(this.$el.find(".datepicker_end").val() !== "date end") {
                new_task.data_end = this.$el.find(".datepicker_end").val();
            }
            //new_task.important_flag = this.$el.find(".add_task .ui-icon-star").css("background-image").slice(-19,-13) === "cd0a0a";
            new_task.important_flag = taskImportantMark.get(this);
            this.model.set(new_task,{validate: true});
        },

        toggleImportantMark: function() {
            taskImportantMark.toggle(this);
        }
    });

    var newTask = new App.Models.newTask();
    var newTaskView = new App.Views.newTask({model: newTask});

    //start preparatory operations
    function after_load() {
        var toDoList_exist = localStorage.getItem('toDoList_exist');
        if(!toDoList_exist) {
            toDoList_exist = true;
            localStorage.setItem('toDoList_exist', toDoList_exist);
            toDoList_task_counter = 0;
            localStorage.setItem('toDoList_task_counter', toDoList_task_counter);
        } else {
            var task_number = Number(localStorage.toDoList_task_counter);
            for (var i = 1; i <= task_number; i++) {
                var task_obj = JSON.parse(localStorage.getItem("toDoList_task_number_" + i));
                if (task_obj == null){
                    continue;
                }
                if ($("." + task_obj.section + " .placeholder").css("display") == "block") {
                    $("." + task_obj.section + " .placeholder").css("display","none");
                }

                task_obj.task_number = i;
                var task = new App.Models.task(task_obj);
                var taskView = new App.Views.task({model: task});
                //tasksCollection.add(task);
            }
        }
        //console.log(tasksCollection.toJSON());

        dialog_default = {
            resizable: false,
            height:140,
            modal: true,
            autoOpen: false
        };

        $(template('confirmation_delete_template')()).dialog(
            $.extend(
                dialog_default,
                {
                    buttons: {
                        "Delete task": function () {
                            $(this).dialog("close");
                            tmp_this.deleteTask();
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                }
            )
        );
        $(template('warning_empty_title_template')()).add(template('warning_editing_mode_template')()).dialog(
            $.extend(
                dialog_default,
                {
                    buttons: {
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                }
            )
        );

        $(document).tooltip({
            show: { delay: 800 }
        });
    }
    after_load();
    //end preparatory operations

});





